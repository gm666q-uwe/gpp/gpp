namespace GPP
{
    public interface IBezierCurveFollower
    {
        BezierCurve Curve { get; set; }

        float CurrentT { get; set; }
    }
}