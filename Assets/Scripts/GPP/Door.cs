using UnityEngine;

namespace GPP
{
    [RequireComponent(typeof(Animator))]
    public class Door : MonoBehaviour
    {
        private static readonly int AnimatorMove = Animator.StringToHash("Move");
        private Animator _animator;
        [SerializeField] private GameObject camera;
        [SerializeField] private GameObject cameraMain;
        public bool IsMoving { get; private set; }

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void Toggle()
        {
            if (IsMoving)
            {
                return;
            }

            cameraMain.SetActive(false);
            camera.SetActive(true);

            IsMoving = true;
            _animator.SetBool(AnimatorMove, IsMoving);
        }

        private void MoveReset()
        {
            cameraMain.SetActive(true);
            camera.SetActive(false);

            IsMoving = false;
            _animator.SetBool(AnimatorMove, IsMoving);
        }
    }
}