using System;
using UnityEngine;

namespace GPP
{
    public class Platform : MonoBehaviour, IBezierCurveFollower
    {
        [SerializeField] private bool bounce = true;
        [SerializeField] private bool normalizeSpeed = true;
        [SerializeField] private float pause = 0.0f;
        private float _pauseCounter = 0.0f;
        private bool _isPaused = false;
        [SerializeField] private bool rotate = true;
        [SerializeField] private float speed = 1.0f;
        [SerializeField] private BezierCurveSpline spline;

        #region IBezierCurveFollower Members

        public BezierCurve Curve
        {
            get { return spline.curve; }
            set { throw new Exception("Set is unsupported"); }
        }

        public float CurrentT { get; set; }

        #endregion

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                other.transform.parent = transform;
            }
        }

        private void OnCollisionExit(Collision other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                other.transform.parent = null;
            }
        }

        private void SetPositionAndRotation(float t)
        {
            var oldPosition = transform.position;
            transform.position = spline.curve.GetPosition(t);
            if (rotate)
            {
                transform.rotation = Quaternion.LookRotation(transform.position - oldPosition);
            }
        }

        private void Update()
        {
            if (_isPaused)
            {
                _pauseCounter += Time.deltaTime;
                if (!(_pauseCounter >= pause)) return;
                _isPaused = false;
                _pauseCounter = 0.0f;
            }
            else
            {
                if (normalizeSpeed)
                {
                    CurrentT += speed * Time.deltaTime / spline.curve.GetTangent(CurrentT).magnitude;
                }
                else
                {
                    CurrentT += speed * Time.deltaTime;
                }

                if (CurrentT > spline.curve.SegmentCount)
                {
                    if (bounce)
                    {
                        CurrentT = spline.curve.SegmentCount - (CurrentT - spline.curve.SegmentCount);
                        speed = -speed;
                        if (pause > 0.0f)
                        {
                            _isPaused = true;
                            SetPositionAndRotation(spline.curve.SegmentCount);
                            return;
                        }
                    }
                    else
                    {
                        CurrentT -= spline.curve.SegmentCount;
                        if (pause > 0.0f)
                        {
                            _isPaused = true;
                            SetPositionAndRotation(spline.curve.SegmentCount);
                            return;
                        }
                    }
                }

                if (CurrentT < 0.0f)
                {
                    if (bounce)
                    {
                        CurrentT = -CurrentT;
                        speed = -speed;
                        if (pause > 0.0f)
                        {
                            _isPaused = true;
                            SetPositionAndRotation(0.0f);
                            return;
                        }
                    }
                    else
                    {
                        CurrentT = spline.curve.SegmentCount + CurrentT;
                        if (pause > 0.0f)
                        {
                            _isPaused = true;
                            SetPositionAndRotation(0.0f);
                            return;
                        }
                    }
                }

                SetPositionAndRotation(CurrentT);
            }
        }
    }
}