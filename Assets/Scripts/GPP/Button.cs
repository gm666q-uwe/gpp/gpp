using UnityEngine;

namespace GPP
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(Collider))]
    public class Button : MonoBehaviour
    {
        private static readonly int AnimatorPress = Animator.StringToHash("Press");
        private Animator _animator;
        private ThirdPersonControllerPlayer _controller;
        [SerializeField] private Door door;

        private void OnTriggerEnter(Collider other)
        {
            _controller = other.GetComponent<ThirdPersonControllerPlayer>();
        }

        private void OnTriggerExit(Collider other)
        {
            _controller = null;
        }

        private void OnTriggerStay(Collider other)
        {
            if (_controller == null || !_controller.IsAttacking) return;
            if (!door.IsMoving)
            {
                _animator.SetTrigger(AnimatorPress);
            }

            door.Toggle();
        }

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }
    }
}