using System;
using UnityEngine;

namespace GPP
{
    public class SimpleFollower : MonoBehaviour, IBezierCurveFollower
    {
        [SerializeField] private bool destroyOnEnd = false;
        [SerializeField] private float speed = 1.0f;
        [SerializeField] private BezierCurveSpline spline;

        #region IBezierCurveFollower Members

        public Vector2 CameraRotation { get; set; }

        public BezierCurve Curve
        {
            get { return spline.curve; }
            set { throw new Exception("Set is unsupported"); }
        }

        public float CurrentT { get; set; }

        #endregion

        private void Update()
        {
            CurrentT += Time.deltaTime * speed / spline.curve.GetTangent(CurrentT).magnitude;
            if (CurrentT > spline.curve.SegmentCount)
            {
                if (!destroyOnEnd)
                {
                    CurrentT -= spline.curve.SegmentCount;
                }
                else
                {
                    Destroy(gameObject);
                    return;
                }
            }

            if (CurrentT < 0.0f)
            {
                CurrentT = spline.curve.SegmentCount - CurrentT;
            }

            var oldPosition = transform.position;
            transform.position = spline.curve.GetPosition(CurrentT);
            transform.rotation = Quaternion.LookRotation(transform.position - oldPosition);
        }
    }
}