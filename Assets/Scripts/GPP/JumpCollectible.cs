using UnityEngine;

namespace GPP
{
    public class JumpCollectible : Collectible
    {
        [SerializeField] private float time;
        [SerializeField] private float value;

        public override Data getData()
        {
            return new Data(time, Type.Jump, value);
        }
    }
}