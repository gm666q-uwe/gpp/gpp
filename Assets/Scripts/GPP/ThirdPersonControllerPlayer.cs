using UnityEngine;

namespace GPP
{
    [RequireComponent(typeof(ThirdPersonController))]
    public class ThirdPersonControllerPlayer : MonoBehaviour
    {
        private Transform _cameraTransform;
        private ThirdPersonController _controller;
        //private Curve curve;
        private float curveT;
        private bool followCurve;
        public bool IsAttacking { get; protected set; }
        public bool IsJumping { get; protected set; }

        private void FixedUpdate()
        {
            var h = Input.GetAxis("Horizontal");
            var v = Input.GetAxis("Vertical");
            if (followCurve)
            {
                curveT += Time.fixedDeltaTime * 0.5f * h;
                if (curveT > 1.0f || curveT < 0.0f)
                {
                    followCurve = false;
                }

                /*transform.position = Mathf.Pow(1 - curveT, 3.0f) * curve.controlPoints[0].position +
                                     3 * Mathf.Pow(1 - curveT, 2) * curveT * curve.controlPoints[1].position +
                                     3 * (1 - curveT) * Mathf.Pow(curveT, 2) * curve.controlPoints[2].position +
                                     Mathf.Pow(curveT, 3) * curve.controlPoints[3].position;*/
            }
            else
            {
                Vector3 move;

                if (_cameraTransform != null)
                {
                    var cameraForward = Vector3.Scale(_cameraTransform.forward, new Vector3(1.0f, 0.0f, 1.0f))
                        .normalized;
                    move = v * cameraForward + h * _cameraTransform.right;
                }
                else
                {
                    move = v * Vector3.forward + h * Vector3.right;
                }

                _controller.Move(move, IsJumping, IsAttacking);
                IsJumping = false;
            }
        }

        private void Start()
        {
            if (Camera.main != null)
            {
                _cameraTransform = Camera.main.transform;
            }

            _controller = GetComponent<ThirdPersonController>();
        }

        private void Update()
        {
            if (!IsAttacking)
            {
                IsAttacking = Input.GetButtonDown("Fire1");
            }

            if (!IsJumping)
            {
                IsJumping = Input.GetButtonDown("Jump");
            }
        }

        /*public void setCurve(Curve curve, float curveT)
        {
            if (followCurve) return;
            this.curve = curve;
            this.curveT = curveT;
            followCurve = true;
        }*/
    }
}