using UnityEngine;

namespace GPP
{
    public class SpeedCollectible : Collectible
    {
        [SerializeField] private float time;
        [SerializeField] private float value;

        public override Data getData()
        {
            return new Data(time, Type.Speed, value);
        }
    }
}