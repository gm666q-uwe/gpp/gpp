using UnityEngine;

namespace GPP
{
    public abstract class Collectible : MonoBehaviour
    {
        #region Type enum

        public enum Type
        {
            Jump,
            Speed,
        }

        #endregion

        public abstract Data getData();

        #region Nested type: Data

        public class Data
        {
            public float Time;
            public Type Type;
            public float Value;

            public Data(float time, Type type, float value)
            {
                Time = time;
                Type = type;
                Value = value;
            }
        }

        #endregion
    }
}