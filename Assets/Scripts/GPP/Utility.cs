using System;

namespace GPP
{
    internal static class Utility
    {
        public static bool DoubleCompare(double lhs, double rhs)
        {
            return Math.Abs(lhs - rhs) <= double.Epsilon * Math.Max(1.0, Math.Max(Math.Abs(lhs), Math.Abs(rhs)));
        }

        public static bool FloatCompare(float lhs, float rhs)
        {
            return Math.Abs(lhs - rhs) <= float.Epsilon * Math.Max(1.0f, Math.Max(Math.Abs(lhs), Math.Abs(rhs)));
        }
    }
}