using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GPP
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(Rigidbody))]
    public class ThirdPersonController : MonoBehaviour
    {
        private static readonly int AnimatorInputZ = Animator.StringToHash("InputZ");
        private static readonly int AnimatorInputX = Animator.StringToHash("InputX");
        private static readonly int AnimatorIsGrounded = Animator.StringToHash("IsGrounded");
        private static readonly int AnimatorAttack = Animator.StringToHash("Attack");
        private Animator _animator;
        private List<Collectible.Data> _collectibles = new List<Collectible.Data>();
        private Vector3 _groundNormal = Vector3.up;
        private float _initialGroundRayDistance;
        private float _initialJumpPower;
        private float _initialMoveSpeed;
        private bool _isGrounded;
        private Rigidbody _rigidbody;
        [SerializeField] private float groundRayDistance = 0.1f;
        [SerializeField] private float jumpPower = 15.0f;
        [SerializeField] private float moveSpeed = 1.0f;

        private void CheckGroundStatus()
        {
            RaycastHit hitInfo;

            Debug.DrawRay(transform.position + Vector3.up * 0.1f, Vector3.down * groundRayDistance, Color.green);

            if (Physics.Raycast(transform.position + Vector3.up * 0.1f, Vector3.down, out hitInfo, groundRayDistance))
            {
                Debug.DrawRay(transform.position + Vector3.up * 0.1f, Vector3.down * hitInfo.distance, Color.red);
                _animator.applyRootMotion = true;
                _groundNormal = hitInfo.normal;
                _isGrounded = true;
            }
            else
            {
                _animator.applyRootMotion = false;
                _groundNormal = Vector3.up;
                _isGrounded = false;
            }
        }

        public void Move(Vector3 move, bool jump, bool attack)
        {
            move = move.magnitude > 1.0f ? move.normalized : move;
            move = transform.InverseTransformDirection(move);
            CheckGroundStatus();
            move = Vector3.ProjectOnPlane(move, _groundNormal);
            var turnAmount = Mathf.Atan2(move.x, move.z);
            var forwardAmount = move.z;

            var turnSpeed = Mathf.Lerp(360.0f, 180.0f, turnAmount);
            //var turnSpeed = Mathf.Lerp(180.0f, 90.0f, turnAmount);
            transform.Rotate(0.0f, turnAmount * turnSpeed * Time.deltaTime, 0.0f);

            if (_isGrounded)
            {
                if (jump)
                {
                    _animator.applyRootMotion = false;
                    groundRayDistance = 0.1f;
                    _isGrounded = false;

                    var power = _collectibles.Any(data => data.Type == Collectible.Type.Jump)
                        ? jumpPower * _collectibles.Where(data => data.Type == Collectible.Type.Jump)
                            .Sum(data => data.Value)
                        : jumpPower;
                    _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, power, _rigidbody.velocity.z);
                }
            }
            else
            {
                groundRayDistance = _rigidbody.velocity.y < 0 ? _initialGroundRayDistance : 0.01f;
            }

            UpdateAnimator(forwardAmount, turnAmount, attack);
        }

        private void OnAnimatorMove()
        {
            if (!_isGrounded || !(Time.deltaTime > 0)) return;
            var speed = _collectibles.Any(data => data.Type == Collectible.Type.Speed)
                ? moveSpeed * _collectibles.Where(data => data.Type == Collectible.Type.Speed).Sum(data => data.Value)
                : moveSpeed;
            var v = _animator.deltaPosition * speed / Time.deltaTime;

            v.y = _rigidbody.velocity.y;
            _rigidbody.velocity = v;
        }

        private void OnTriggerEnter(Collider other)
        {
            var collectible = other.GetComponent<Collectible>();

            if (collectible == null) return;
            _collectibles.Add(collectible.getData());
            Destroy(other.gameObject);
        }

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _initialGroundRayDistance = groundRayDistance;
            _rigidbody = GetComponent<Rigidbody>();

            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }

        private void Update()
        {
            foreach (var collectible in _collectibles)
            {
                collectible.Time -= Time.deltaTime;
                if (collectible.Time <= 0.0f)
                {
                    _collectibles.Remove(collectible);
                }
            }
        }

        private void UpdateAnimator(float forwardAmount, float turnAmount, bool attack)
        {
            _animator.SetFloat(AnimatorInputZ, forwardAmount, 0.1f, Time.fixedDeltaTime);
            _animator.SetFloat(AnimatorInputX, turnAmount, 0.1f, Time.fixedDeltaTime);
            _animator.SetBool(AnimatorIsGrounded, _isGrounded);

            _animator.SetBool(AnimatorAttack, attack);
        }
    }
}