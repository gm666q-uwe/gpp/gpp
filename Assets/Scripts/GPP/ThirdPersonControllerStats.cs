using System.Collections.Generic;
using UnityEngine;

namespace GPP
{
    public class ThirdPersonControllerStats : MonoBehaviour
    {
        private List<Effect> _effects;

        public void addEffect(Effect effect)
        {
            _effects.Add(effect);
        }
    }
}